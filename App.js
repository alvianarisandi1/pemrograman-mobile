import React, { useState } from "react";
import { SafeAreaView, View, Image, StyleSheet , Text, TextInput,ScrollView,StatusBar,Switch,TouchableWithoutFeedback} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#6ce678",
    padding: 10
  },
  
  logo: {
    width: 66,
    height: 58,
  },
  baseText: {
    fontFamily: "Cochin"
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold"
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    backgroundColor: '#F6',
    marginHorizontal: 20,
  },
  text: {
    fontSize: 18,
    alignSelf:'center',
    marginTop:25,
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  countContainer: {
    alignItems: "center",
    padding: 10,
    },
  countText: {
    color: "red"
  },
  switcha :{
    false : "red",
    true : "blue",
  }
  

});
const tampil = () => {
  const [text, onChangeText] = React.useState(null);
  const [number, onChangeNumber] = React.useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch= () => setIsEnabled(previousState => !previousState); 
  const[count, setCount] = useState(0);
const onPress= () => {
  setCount(count + 1) ;
}; 

  return (
    
    < SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>

     <View style={styles.container}>
     <Switch trackColor={styles.switcha}
     thumbColor={isEnabled ? "green" : "blue"}
     ios_backgroundColor="white"
     onValueChange={toggleSwitch}value={isEnabled}/>
<View style={styles.countContainer}>           
<Text style={styles.countText}>Terhitung: {count}</Text>
         </View>         
<TouchableWithoutFeedback onPress={onPress}>
           <View style={styles.button}>
           <Text style={styles.TouchText}>Pencet Aku</Text>
           </View>           
</TouchableWithoutFeedback> 




     <Text style={styles.text}>
              TUGAS MINGGU 5 ALVIAN ARISANDI
      </Text>
     <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder=" Masukkan Text"
      
      />

      
      <TextInput
        style={styles.input}
        onChangeText={onChangeNumber}
        value={number}
        placeholder=" Masukkan Angka"
        keyboardType="numeric"
      />
      <Image
        style={styles.logo}
        source={{
          uri: 'https://img.tek.id/img/content/2020/08/11/31957/nickelodeon-garap-serial-tv-spin-off-patrick-6siywPEb7g.jpg',
        }}
        style={{width:400,height:200,alignSelf:'center',marginTop:20}}
      />
      <Image
        style={styles.logo}
        source={{
          uri: 'https://img.tek.id/img/content/2020/08/11/31957/nickelodeon-garap-serial-tv-spin-off-patrick-6siywPEb7g.jpg',
        }}
        style={{width:400,height:200,alignSelf:'center',marginTop:20}}
      />
     
        <Text style={styles.title}>React Native</Text>


    </View>
    </ScrollView>  
    </SafeAreaView>
  )};



export default tampil;